package com.example.footballclub.fragment;


import android.content.res.TypedArray;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.footballclub.R;
import com.example.footballclub.adapter.ClubAdapter;
import com.example.footballclub.model.Club;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ClubFragment extends Fragment {
    private String[] judul, deskripsi;
    private TypedArray logo;
    private ClubAdapter clubAdapter;
    private RecyclerView rvClub;
    private ArrayList<Club> listClub = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_club,container,false);

        clubAdapter = new ClubAdapter(getContext());
        rvClub = view.findViewById(R.id.rvClub);
        rvClub.setLayoutManager(new LinearLayoutManager(getContext()));
        rvClub.setAdapter(clubAdapter);

        addItem();

        return view;
    }

    private void addItem() {
        judul = getResources().getStringArray(R.array.judul_club);
        deskripsi = getResources().getStringArray(R.array.deskripsi_club);
        logo = getResources().obtainTypedArray(R.array.logo_club);
        listClub = new ArrayList<>();

        for (int i = 0; i < judul.length; i++){
            Club club = new Club();
            club.setJudul(judul[i]);
            club.setDeskripsi(deskripsi[i]);
            club.setLogo(logo.getResourceId(i, -1 ));
            listClub.add(club);

        }
        clubAdapter.setListClub(listClub);
    }

}
