package com.example.footballclub.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.footballclub.DetailActivity;
import com.example.footballclub.R;
import com.example.footballclub.model.Club;

import java.util.ArrayList;

public class ClubAdapter extends RecyclerView.Adapter<ClubAdapter.ClubViewHolder> {
    private Context context;
    private ArrayList<Club> listClub;

    public ClubAdapter(Context context) {this.context = context;}

    public ArrayList<Club> getListClub() { return listClub; }

    public void setListClub(ArrayList<Club> listClub) { this.listClub = listClub; }

    @NonNull
    @Override
    public ClubAdapter.ClubViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item,parent,false);
        return new ClubViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ClubAdapter.ClubViewHolder holder, final int position) {
    holder.tvJudul.setText(getListClub().get(position).getJudul());
    holder.ivGambar.setImageResource(getListClub().get(position).getLogo());

    holder.itemView.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View view) {
        Intent intent = new Intent (context, DetailActivity.class);
        intent.putExtra(DetailActivity.EXTRA_CLUB, listClub.get(position));
        context.startActivity(intent);

        }
    });

    }

    @Override
    public int getItemCount()  { return getListClub().size(); }

    public class ClubViewHolder extends RecyclerView.ViewHolder{
        TextView tvJudul;
        ImageView ivGambar;

        public ClubViewHolder (@NonNull View itemView) {
            super(itemView);

            tvJudul = itemView.findViewById(R.id.tvJudul);
            ivGambar = itemView.findViewById(R.id.ivGambar);
        }
    }

}
