package com.example.footballclub;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.footballclub.model.Club;

public class DetailActivity extends AppCompatActivity {

    public static final String EXTRA_CLUB = "extra_club";

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView tvJudul, tvOverview;
        ImageView ivGambar;

        tvJudul = findViewById(R.id.tvNamaClub);
        tvOverview = findViewById(R.id.tvOverview);
        ivGambar = findViewById(R.id.ivLogoClub);

        Club club = getIntent().getParcelableExtra(EXTRA_CLUB);
        setTitle(club.getJudul());

        tvJudul.setText(club.getJudul());
        tvOverview.setText(club.getDeskripsi());
        ivGambar.setImageResource(club.getLogo());

    }
}
